# ToDoApi
consultas Docker
Levantar Docker Compose
//esto se debe hacer en la carpeta especifica de ToDoApi (Contiene proyecto backend)
docker-compose up --build 
//Verificamos que el contenedor este corriendo
docker ps

Abrir BD docker: 
ejemplo:
docker exec -it 7eb4e80ca6a2 mysql -u root -p
Password: example
Use ToDoApp;

Solo usuarios que tengan tareas asignadas:

SELECT 
    u.ID AS UsuarioID, 
    u.Nombre, 
    u.Email, 
    t.ID AS TareaID, 
    t.Descripcion, 
    t.Fecha
FROM 
    Usuarios u
JOIN 
    Tareas t ON u.ID = t.UsuarioID;
	
	
	
	
Carga usuarios aunque no tenga tareas

SELECT 
    u.ID AS UsuarioID, 
    u.Nombre, 
    u.Email, 
    t.ID AS TareaID, 
    t.Descripcion, 
    t.Fecha
FROM 
    Usuarios u
LEFT JOIN 
    Tareas t ON u.ID = t.UsuarioID;

